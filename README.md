# Migrate Expanded

Provides migrate source plugins that load _complete_ entities, where
the entities referenced by paragraph and field collection fields are
recursively loaded.

## Prerequisites

This module expects a D7 database to exist and be available using the
`migrate` key. In `settings.php` (or similar), you should have something
like this alongside `$databases['default']['default']`:

```php
// Migration database
$databases['migrate']['default'] = [
  'database' => 'name_of_d7_database',
  'username' => 'somebody',
  'password' => 'secret',
  'host' => 'db',
  'driver' => 'mysql',
];
```

## D7FieldTypes service

Service that can examine the `migrate` database and provide information about
the type of each field.

## Migration source plugins

The `ExpandedNode`, `ExpandedParagraphsItem` and `ExpandedFieldCollectionItem`
source plugins work like their core counterparts, but also recursively load
_child entities_.

Paragraph and field collection entities are considered children. The entity
that references them is considered their parent.

This should save you some work at the migration process level, because you
can deal with the content of a child entity without having to load it
separately. It also helps when you want to flatten a node+paragraph or
paragraph+paragraph data structure into a single target entity.

For example, consider a D7 node with a `field_body_paragraphs` field that
references a `cta` paragraph, which itself has title and link. The core
`d7_node` migration source will provide data like this:

```php
'field_body_paragraphs' => [
  0 => [
    'target_id' => 123,
    'revision_id' => 456,
  ],
]
```

The `migrate_expanded_d7_node` migration source will provide data like this:

```php
'field_body_paragraphs' => [
  0 => [
    'target_id' => 123,
    'revision_id' => 456,
    'bundle' => 'cta',
    'field_cta_title' => 'Subscribe now!',
    'field_cta_link' => 'https://www.example.com/subscribe',
  ],
]
```

This recursive, so paragraphs within paragraphs are loaded too.

## Paragraphs per bundle/field

The `ExpandedParagraphsItem` source plugin can be configured to only accept
paragraphs for a particular field.

For example, this source will load all of the `cta` paragraphs that are
children of `event` nodes and referenced by the `field_body_paragraphs`
field:

```yaml
source:
  plugin: migrate_expanded_d7_paragraphs_item
  bundle: cta
  parents:
    - entity_type: node
      bundle: event
      field: field_body_paragraphs
```

You may need this if your D7 site has content moderation (see below).

### Content moderation and paragraphs

If your D7 site uses both content moderation and paragraphs, an interesting
nd potentially tricky scenario arises. Every revisionable entity in Drupal
has the concept of a _current_ revision. For nodes, this is denoted by the
`vid` column in the `node` table.

Without content moderation, we can assume this is the most recent revision,
(with the highest `vid` value). But with content moderation, there are two
different definition of _current_:

1. the published revision (denoted by `vid`)
2. the most recent revision (which may be a later draft)

For nodes, this is fine - the field data is read from the `field_revision_`
tables and the node's `vid` is used.

For paragraphs, there is a problem. Paragraphs also have a _current revision_
concept (via the `revision_id` column on the `paragraphs_item` table), but
only their most recent revision. To get the published revision, we need to
query the parent node (or the grandparent, great-grandparent, etc).

To migrate paragraphs where content moderation is used, add the `parents`
property to the source.

## D7EntityLoader service

Service that can load a complete entity from the `migrate` database,
including all its child entities. It uses the plugins listed above to
get its data, and the plugins themselves use this to get child entities.
