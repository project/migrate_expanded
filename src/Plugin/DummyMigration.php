<?php

namespace Drupal\migrate_expanded\Plugin;

use Drupal\migrate\Plugin\Migration;
use Drupal\migrate\Plugin\MigrationInterface;

class DummyMigration extends Migration {

  protected int $status = MigrationInterface::STATUS_IDLE;
  protected int $interruptionResult = MigrationInterface::RESULT_INCOMPLETE;

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getInterruptionResult() {
    return $this->interruptionResult;
  }

  /**
   * {@inheritdoc}
   */
  public function clearInterruptionResult() {
    $this->interruptionResult = MigrationInterface::RESULT_INCOMPLETE;
  }

  /**
   * {@inheritdoc}
   */
  public function interruptMigration($result) {
    $this->setStatus(MigrationInterface::STATUS_STOPPING);
    $this->interruptionResult = $result;
  }

}
