<?php

namespace Drupal\migrate_expanded\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate_expanded\FieldIdentifier;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * @MigrateSource(
 *   id = "migrate_expanded_d7_paragraphs_item"
 * )
 */
class ExpandedParagraphsItem extends FieldableEntity implements ExpandedFieldableEntityInterface {

  use FieldableEntityExpansionTrait;

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'item_id' => $this->t('The paragraph_item id'),
      'revision_id' => $this->t('The paragraph_item revision id'),
      'bundle' => $this->t('The paragraph bundle'),
      'field_name' => $this->t('The paragraph field_name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'item_id' => [
        'type' => 'integer',
        'alias' => 'p',
      ],
    ];
  }


  public function getEntityType(): string {
    return 'paragraphs_item';
  }


  /**
   * {@inheritdoc}
   */
  public function query() {
    $parents = $this->configuration['parents'] ?? [];
    $query = $parents
      ? $this->queryWithParents($parents)
      : $this->queryWithoutParents();

//    print "----------------------------------------------------------------------------------\n";
//    print "$query\n";
//    print "----------------------------------------------------------------------------------\n";
//    print_r($query->getArguments());
//    print "\n";
//    print "----------------------------------------------------------------------------------\n";

    return $query;
  }


  protected function queryWithoutParents(): SelectInterface {
    $query = $this->select('paragraphs_item', 'p');
    $query->addField('p', 'item_id');
    $query->addField('p', 'bundle');

    $bundle = $this->configuration['bundle'] ?? '';
    if ($bundle) {
      $query->condition('p.bundle', $bundle);
    }

    if ($this->configuration['item_id']) {
      $query->condition('p.item_id', $this->configuration['item_id']);
    }
    if ($this->configuration['revision_id']) {
      $query->addField('r', 'revision_id');
      $query->innerJoin('paragraphs_item_revision', 'r', 'r.item_id=p.item_id');
      $query->condition('r.revision_id', $this->configuration['revision_id']);
    }

    return $query;
  }


  protected function queryWithParents(array $parents): SelectInterface {
    /** @var FieldIdentifier[] $identifiers */
    $identifiers = array_map(fn($parent) => FieldIdentifier::fromConfiguration($parent), $parents);

    // Start of as normal:
    //   SELECT r.item_id
    //   FROM paragraphs_item_revision
    //   INNER JOIN paragraphs_item p ON p.item_id=r.item_id
    //   WHERE p.bundle='row'

    $query = $this->select('paragraphs_item_revision', 'r');
    $query->innerJoin('paragraphs_item', 'p', 'r.item_id=p.item_id');
    $query->fields('r', ['item_id', 'revision_id']);

    $bundle = $this->configuration['bundle'] ?? '';
    if ($bundle) {
      $query->condition('p.bundle', $bundle);
    }

    // Work our way up the hierarchy via the field_revision_ tables:
    //   INNER JOIN field_revision_field_table f0 ON f0.field_table_revision_id=r.revision_id
    //   WHERE f0.bundle='accordion_item'
    //   INNER JOIN field_revision_field_accordion_item f1 ON f1.field_accordion_item_revision_id=f0.revision_id
    //   WHERE f1.bundle='accordion'
    //   INNER JOIN field_revision_field_paragraphs_content f2 ON f2.field_paragraphs_content_revision_id=f1.revision_id
    //   WHERE f2.bundle='page'

    $revisionIdExpression = 'r.revision_id';
    foreach (array_reverse($identifiers) as $i) {
      $table = $i->fieldRevisionTable();
      $condition = "%alias.{$i->fieldRevisionRevisionIdColumn()} = $revisionIdExpression";
      $alias = $query->innerJoin($table, 'f', $condition);
      $revisionIdExpression = "{$alias}.revision_id";
      if ($i->bundle) {
        $query->condition("{$alias}.bundle", $i->bundle);
      }
    }

    // Last inner join looks a bit like this:
    //   INNER JOIN node n ON n.nid=f2.entity_id AND n.vid=f2.revision_id
    // This gets us the top level entity (usually a node, but it doesn't have
    // to be). We're interested in the published revision of this top level
    // node.

    $top = $identifiers[0];
    $entityIdMatch = "%alias.{$top->idColumn()} = {$alias}.entity_id";
    $entityRevisionIdMatch = "%alias.{$top->revisionIdColumn()} = {$alias}.revision_id";
    $query->innerJoin($top->table(), 'n', "$entityIdMatch AND $entityRevisionIdMatch");

    return $query;
  }


  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    if ($result) {

      $bundle = $row->getSourceProperty('bundle');
      $entityId = $row->getSourceProperty('item_id');
      $revisionId = $row->getSourceProperty('revision_id');
      foreach (array_keys($this->getFields('paragraphs_item', $bundle)) as $fieldName) {
        $row->setSourceProperty($fieldName, $this->getFieldValues('paragraphs_item', $fieldName, $entityId, $revisionId));
      }

      $this->loadChildren($row);
    }
    return $result;
  }

}
