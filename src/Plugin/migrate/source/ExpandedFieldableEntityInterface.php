<?php

namespace Drupal\migrate_expanded\Plugin\migrate\source;

interface ExpandedFieldableEntityInterface {

  public function getEntityType(): string;

}
