<?php

namespace Drupal\migrate_expanded\Plugin\migrate\source;

use Drupal\migrate_expanded\D7EntityLoader;
use Drupal\migrate_expanded\D7FieldTypes;
use Drupal\migrate\Row;

/**
 * Provides functionality to fully load child entities.
 *
 * This trait is for use with migrate source plugins. To use it, call
 * the functions from your source plugin's prepareRow function.
 */
trait FieldableEntityExpansionTrait {

  /**
   * Where a source value is a reference to a paragraph or field collection,
   * that reference will be replaced with the complete data for the referenced
   * entity.
   *
   * Example:
   *   'field_items' => ['value' => 5, 'revision_id' => 10]
   *
   * becomes:
   *   'field_items' => [
   *      'item_id' => 5,
   *      'revision_id' => 10,
   *      'title' => 'The paragraph title',
   *      // rest of the paragraph fields here
   *   ]
   *
   * @param Row $row
   */
  protected function loadChildren(Row $row): void {
    /** @var D7EntityLoader $entityLoader */
    $d7EntityLoader = \Drupal::service('migrate_expanded.d7_entity_loader');
    /** @var D7FieldTypes $fieldTypes */
    $d7FieldTypes = \Drupal::service('migrate_expanded.d7_field_types');

    foreach ($row->getSource() as $key => $values) {
      $fieldType = $d7FieldTypes->getFieldType($this->getEntityType(), $key);

      if ($fieldType === 'paragraphs') {
        foreach ($values as $delta => $value) {
          $itemId = $value['value'];
          $revisionId = $value['revision_id'];
          $child = $d7EntityLoader->load('paragraphs_item', $itemId, $revisionId);
          $values[$delta] = $child ? $child : ['item_id' => $itemId, 'revision_id' => $revisionId];
        }
        $row->setSourceProperty($key, $values);
      }

      elseif ($fieldType === 'field_collection') {
        foreach ($values as $delta => $value) {
          $itemId = $value['value'];
          $revisionId = $value['revision_id'];
          $child = $d7EntityLoader->load('field_collection_item', $itemId, $revisionId);
          $values[$delta] = $child ? $child : ['item_id' => $itemId, 'revision_id' => $revisionId];
        }
        $row->setSourceProperty($key, $values);
      }

    }
  }


}
