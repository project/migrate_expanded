<?php

namespace Drupal\migrate_expanded\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\paragraphs\Plugin\migrate\source\d7\FieldCollectionItem;

/**
 * @MigrateSource(
 *   id = "migrate_expanded_d7_field_collection_item",
 * )
 */
class ExpandedFieldCollectionItem extends FieldCollectionItem implements ExpandedFieldableEntityInterface {

  use FieldableEntityExpansionTrait;

  // This is used within FieldCollectionItem::query().
  // We override it here and replace it with a join that includes
  // all revisions. That way, we can add a WHERE clause
  // to the query that will fetch either a specific revision,
  // or the current revision.
  const JOIN = 'f.item_id = fr.item_id';


  public function getEntityType(): string {
    return 'field_collection_item';
  }

  public function query() {
    $query = parent::query();
    if (isset($this->configuration['item_id'])) {
      $query->condition('f.item_id', $this->configuration['item_id']);
    }

    if (isset($this->configuration['revision_id'])) {
      $query->condition('fr.revision_id', $this->configuration['revision_id']);
    }
    else {
      // If no specific revision was asked for, limit the results to
      // one revision - the current revision.
      $query->where('f.revision_id=fr.revision_id');
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    if ($result) {

      $bundle = $row->getSourceProperty('bundle');
      $entityId = $row->getSourceProperty('item_id');
      $revisionId = $row->getSourceProperty('revision_id');
      foreach (array_keys($this->getFields('paragraphs_item', $bundle)) as $fieldName) {
        $row->setSourceProperty($fieldName, $this->getFieldValues('paragraphs_item', $fieldName, $entityId, $revisionId));
      }

      $this->loadChildren($row);
    }
    return $result;
  }


}
