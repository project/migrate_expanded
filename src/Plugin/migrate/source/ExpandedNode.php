<?php

namespace Drupal\migrate_expanded\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * @MigrateSource(
 *   id = "migrate_expanded_d7_node",
 * )
 */
class ExpandedNode extends Node implements ExpandedFieldableEntityInterface {

  use FieldableEntityExpansionTrait;

  public function getEntityType(): string {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    if (isset($this->configuration['nid'])) {
      $query->condition('n.nid', $this->configuration['nid']);
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    if ($result) {
      $this->loadChildren($row);
    }
    return $result;
  }

}
