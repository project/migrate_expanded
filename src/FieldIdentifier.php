<?php

namespace Drupal\migrate_expanded;

class FieldIdentifier {

  public string $entityType;
  public ?string $bundle;
  public string $fieldName;


  function __construct(string $entityType, ?string $bundle, string $fieldName) {
    $this->entityType = $entityType;
    $this->bundle = $bundle;
    $this->fieldName = $fieldName;
  }


  function table() {
    switch ($this->entityType) {
      case 'node':
        return 'node';
      case 'paragraphs_item':
        return 'paragraphs_item';
      case 'field_collection_item':
        return 'field_collection_item';
      case 'bean':
        return 'bean';
      default:
        throw new \RuntimeException("Table name is unknown for entity type {$this->entityType}");
    }
  }

  function bundleColumn() {
    switch ($this->entityType) {
      case 'node':
        return 'type';
      case 'paragraphs_item':
        return 'bundle';
      case 'field_collection_item':
        return 'field_name';
      case 'bean':
        return 'type';
      default:
        throw new \RuntimeException("Bundle column name is unknown for entity type {$this->entityType}");
    }
  }

  function idColumn() {
    switch ($this->entityType) {
      case 'node':
        return 'nid';
      case 'paragraphs_item':
        return 'item_id';
      case 'field_collection_item':
        return 'item_id';
      case 'bean':
        return 'bid';
      default:
        throw new \RuntimeException("ID column is unknown for entity type {$this->entityType}");
    }
  }

  function revisionIdColumn() {
    switch ($this->entityType) {
      case 'node':
        return 'vid';
      case 'paragraphs_item':
        return 'revision_id';
      case 'field_collection_item':
        return 'revision_id';
      case 'bean':
        return 'vid';
      default:
        throw new \RuntimeException("Revision ID column is unknown for entity type {$this->entityType}");
    }
  }

  function fieldRevisionTable() {
    return "field_revision_{$this->fieldName}";
  }

  function fieldRevisionRevisionIdColumn() {
    return "{$this->fieldName}_revision_id";
  }


  /**
   * Create a FieldIdentifier object from an array of properties.
   *
   * @param array $array
   *   Array of the form: [
   *     'entity_type' => 'node',
   *     'bundle' => 'page',   // optional
   *     'field' => 'field_paragraphs',
   *   ]
   *
   * @return FieldIdentifier
   */

  static function fromConfiguration(array $array): FieldIdentifier {
    $entityType = $array['entity_type'] ?? NULL;
    $bundle = $array['bundle'] ?? NULL;
    $fieldName = $array['field'] ?? NULL;
    if (!$entityType || !$fieldName) {
      throw new \InvalidArgumentException("Definition of FieldIdentifier must contain 'entity_type' and 'field' properties");
    }
    return new FieldIdentifier($entityType, $bundle, $fieldName);
  }

//  static function parseString(string $string): FieldIdentifier {
//    $name = "[0-9a-z_]+";
//    $dot = '\.';
//    $pattern = "/^({$name}){$dot}({$name}){$dot}($name)$/";
//
//    $matches = [];
//    if (preg_match($pattern, $string, $matches)) {
//      return new FieldIdentifier($matches[1], $matches[2], $matches[3]);
//    }
//    else {
//      throw new \InvalidArgumentException("Could not parse string: $string");
//    }
//  }
//
//
//  static function parsePath(string $path): array {
//    $orig = $path;
//    $results = [];
//
//    $name = "[0-9a-z_]+";
//    $dot = '\.';
//
//    $pattern = "/^({$name}){$dot}({$name}){$dot}($name)(?:\.(.+))?$/";
//
//    $matches = [];
//    while (preg_match($pattern, $path, $matches)) {
//      $results[] = new FieldIdentifier($matches[1], $matches[2], $matches[3]);
//      $path = $matches[4];
//    }
//
//    if (!empty($path) || empty($results)) {
//      throw new \InvalidArgumentException("Could not parse path: $orig");
//    }
//
//    return $results;
//  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->bundle
      ? "{$this->entityType}.{$this->bundle}.{$this->fieldName}"
      : "{$this->entityType}.{$this->fieldName}";
  }

}
