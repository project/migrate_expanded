<?php

namespace Drupal\migrate_expanded;

use Drupal\migrate_expanded\Plugin\DummyMigration;
use Drupal\migrate\Plugin\MigrateDestinationPluginManager;
use Drupal\migrate\Plugin\MigratePluginManagerInterface;
use Drupal\migrate\Plugin\MigrateSourcePluginManager;
use Drupal\migrate\Plugin\Migration;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Row;

/**
 * Loads Drupal 7 entities as they would be by migrate source plugins.
 */
class D7EntityLoader {

  /** @var MigrationPluginManagerInterface */
  protected $migrationPluginManager;

  /** @var MigratePluginManagerInterface */
  protected $sourcePluginManager;

  /** @var MigratePluginManagerInterface */
  protected $processPluginManager;

  /** @var MigrateDestinationPluginManager */
  protected $destinationPluginManager;

  /** @var MigratePluginManagerInterface */
  protected $idMapPluginManager;


  public function __construct(MigrationPluginManagerInterface $migrationPluginManager,
                              MigrateSourcePluginManager $sourcePluginManager,
                              MigratePluginManagerInterface $processPluginManager,
                              MigrateDestinationPluginManager $destinationPluginManager,
                              MigratePluginManagerInterface $idMapPluginManager) {
    $this->migrationPluginManager = $migrationPluginManager;
    $this->sourcePluginManager = $sourcePluginManager;
    $this->processPluginManager = $processPluginManager;
    $this->destinationPluginManager = $destinationPluginManager;
    $this->idMapPluginManager = $idMapPluginManager;
  }

  /**
   * Load an individual Drupal 7 entity.
   *
   * @param string $entityType
   *   Drupal 7 Entity type.
   * @param int $entityId
   *   Drupal 7 Entity ID.
   * @param int|null $revisionId
   *   Optional Drupal 7 revision ID.
   * @return array
   *   An array as would be loaded by the relevant migrate source plugin,
   *   or an empty array if there is no such entity.
   */
  public function load(string $entityType, int $entityId, ?int $revisionId = NULL): array {
    $migration = $this->createMigrationPlugin($entityType, $entityId, $revisionId);

    $source = $migration->getSourcePlugin();
    $source->rewind();

    $data = [];
    if ($source->valid()) {
      /** @var Row $row */
      $row = $source->current();
      $data = $row->getSource();
    }

    // Remove things that are only to do with migration.
    unset($data['migrate_map_sourceid1']);
    unset($data['migrate_map_source_row_status']);
    unset($data['plugin']);

    return $data;
  }

  /**
   * Creates a temporary migration plugin suitable for loading an entity.
   *
   * This migration cannot be used to actually migrate content, but it is
   * complete enough to iterate over its source and extract data within
   * the Drupal 7 entity.
   */
  protected function createMigrationPlugin(string $entityType, int $entityId, ?int $revisionId): Migration {
    $config = [];
    $config['process'] = [];
    $config['destination'] = ['plugin' => 'null'];

    switch ($entityType) {
      case 'node':
        $config['source'] = [
          'plugin' => 'migrate_expanded_d7_node',
          'nid' => $entityId,
        ];
        break;
      case 'paragraphs_item':
        $config['source'] = [
          'plugin' => 'migrate_expanded_d7_paragraphs_item',
          'item_id' => $entityId,
          'revision_id' => $revisionId,
        ];
        break;
      case 'field_collection_item':
        $config['source'] = [
          'plugin' => 'migrate_expanded_d7_field_collection_item',
          'item_id' => $entityId,
          'revision_id' => $revisionId,
        ];
        break;
      default:
        throw new \RuntimeException("No source plugin for entity type: $entityType");
    }

    return new DummyMigration($config, NULL, [],
      $this->migrationPluginManager,
      $this->sourcePluginManager,
      $this->processPluginManager,
      $this->destinationPluginManager,
      $this->idMapPluginManager);
  }

}
