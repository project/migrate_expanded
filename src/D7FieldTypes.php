<?php

namespace Drupal\migrate_expanded;

use Drupal\Core\Database\Database;

/**
 * Holds data about fields in the Drupal 7 database.
 */
class D7FieldTypes {

  protected array $fieldTypes = [];

  public function __construct() {
    $con = Database::getConnection('default', 'migrate');

    $query = $con->select('field_config_instance', 'fi');
    $query->innerJoin('field_config', 'f', 'f.id=fi.field_id');
    $query->addField('fi', 'entity_type');
    $query->addField('f', 'field_name');
    $query->addField('f', 'type');
    $query->addField('f', 'cardinality');
    $query->distinct();

    foreach ($query->execute()->fetchAll() as $row) {
      $this->fieldTypes[$row->entity_type][$row->field_name] = [
        'type' => $row->type,
        'cardinality' => $row->cardinality,
      ];
    }
  }

  /**
   * The type of the given field.
   *
   * @param string $entityType
   *   The Drupal 7 entity type.
   * @param string $fieldName
   *   The Drupal 7 field name.
   * @return string|null
   *   The Drupal 7 field type, or NULL if not found.
   */
  public function getFieldType(string $entityType, string $fieldName): ?string {
    return $this->fieldTypes[$entityType][$fieldName]['type'] ?? NULL;
  }

  public function getCardinality(string $entityType, string $fieldName): ?int {
    return $this->fieldTypes[$entityType][$fieldName]['cardinality'] ?? NULL;
  }

}
